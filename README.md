FarDOM
======

Interact from Node.js or WebWorker with a distant and inaccessible DOM on the browser main thread.

```plantuml
note "client.full-bundle.mjs" as client_mjs
frame "Far Env" {
  class FarWindow
  client_mjs .. FarWindow : import
  class SomeDOMConnector
  client_mjs .. SomeDOMConnector : import
  object farWin
  FarWindow ..> farWin : new FarWindow(connToDOMSrv)
  object connToDOMSrv
  SomeDOMConnector ..> connToDOMSrv : new SomeDOMConnector()
  connToDOMSrv <--> farWin #line:blue : updates
  note bottom of farWin
    That you can use as a DOM's window.
    You can get the document, query for
    page elements and change then.
  end note
}

note bottom of "Far Env"
  The "Far Env" is a execution environment
  without access to the page DOM. The moust
  likely will be a <b>WebWorker</b>, however that can
  be a backend server or a WebAsm app.
end note

note "dom-srv.full-bundle.mjs" as domsrv_mjs
frame "Web Page" {
  class Sandbox
  domsrv_mjs .. Sandbox : import
  class SomeClientConnector
  domsrv_mjs .. SomeClientConnector : import
  object sandbox
  Sandbox ..> sandbox : el = getElement('#sandbox')\nnew Sandbox(el, srvToClient)
  object connToClient
  SomeClientConnector ..> connToClient : new SomeClientConnector()
  connToClient <--> sandbox #line:blue : updates
  note right of sandbox
    This object is basically
    a DOM server.
  end note
  note bottom of sandbox
    {{
      <style>
        saltDiagram {
          BackgroundColor transparent
        }
      </style>
      salt
      {+
        scale 1.5
        {* File | Edit | History }
        == <b><color:red>This is a Sensible Page
        ..
        {
          {
            Some sensible
            information <&person>
            [<&key> Danger!]
            the "Far Env"
            can't see this.
          } | {
            {^<color:Blue><b><#sandbox>
              <color:Blue>Some information
              [<color:Blue>and interactive]
              [X] <color:Blue>features,
              <color:Blue>from "Far Env"
            }
          }
        }
      }
    }}
  end note
}

connToClient <--> connToDOMSrv #line:blue : updates
```


Install & Run
-------------

```sh
$ git clone git@gitlab.com:nTopus/far-dom.git
$ cd far-dom
$ npm install
$ npm run build
```
* Run some local HTTP server on the project root (current dir).
* Visit the examples dir.
* Have fun.

Hacking
-------

### Human creative mode

Console 1:
```sh
$ cd far-dom
$ npm run build:esm -- -w # this will rebuild the project after each change.
```
Console 2:
```sh
$ cd far-dom
# Run some local HTTP server
# Visit https://localhost:<PORT>/examples/ and see it alive
```
Console 3 (or IDE):
```sh
$ cd far-dom
# Edit files & create wander features & kill bugs...
# See that happening in your browser.
# Update tests to describe and protect your inprovements.
$ npm test # untill all pass
$ git commit -am '<some descriptive summary>'
$ git push
# Send MR to the origin project repo
```

### TDD mode (The better hacking way)

Console 1:
```sh
$ cd far-dom
$ npm run test:watch # this will open the Vitest UI and run the tests after each change.
```
Console 2 (or IDE):
```sh
$ cd far-dom
# Update tests, anticipating the feature or bug that you'll work on.
# Edit files & create wonder features & kill bugs...
# See the tests running in the Vitest Web UI.
$ git commit -am '<some descriptive summary>'
$ git push
# Send MR to the origin project repo
```
