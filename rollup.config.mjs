import rollupTypescript from '@rollup/plugin-typescript'
import tsconfig from './tsconfig.json' assert { type: 'json' }
import fs from 'node:fs'

const clientToDomConnectosDir = './src/client/dom-connectors'
const domToclientConnectosDir = './src/dom-srv/client-connectors'

function inputConfig(input, output) {
	return {
		input,
		output: {
			file: output,
			format: 'es',
	    sourcemap: true
		},
	  plugins: [
	    rollupTypescript(tsconfig)
	  ]
	}
}

export default [
	inputConfig('./src/client/main.ts', './dist/client.mjs'),
	inputConfig('./src/dom-srv/main.ts', './dist/dom-srv.mjs'),
	...fs.readdirSync(clientToDomConnectosDir).map(file =>
		inputConfig(
			clientToDomConnectosDir+'/'+file,
			'./dist/client-to-dom-connector-'+file.replace(/ts$/, 'mjs')
	  )
	),
	...fs.readdirSync(domToclientConnectosDir).map(file =>
		inputConfig(
			domToclientConnectosDir+'/'+file,
			'./dist/dom-to-client-connector-'+file.replace(/ts$/, 'mjs')
	  )
	)
]
