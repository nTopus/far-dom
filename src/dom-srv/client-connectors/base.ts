import Sandbox from '../Sandbox'
import { TTreeState } from 'types/virtual-dom'
import { TUpdateList } from 'types/update-description'

type NFunc = Function & { name: string }

export default class ClientConnectorBase extends EventTarget {

  protected __sandbox?: Sandbox

  constructor() {
    super()
  }

  connect(sandbox: Sandbox) {
    this.__specificConnector()
    this.__sandbox = sandbox
  }

  protected __specificConnector() {
    throw Error(`Missed "__specificConnector()" implementation in "${(this.constructor as NFunc).name}".`)
  }

  protected __weAreConnected() {
    const event = new Event('connected')
    this.dispatchEvent(event)
  }

  queryFullDOMState(): TTreeState {
    if (!this.__sandbox) throw Error('Sandbox was not initialized.')
    return this.__sandbox.queryFullDOMState()
  }

  applyUpdates(updates: TUpdateList) {
    const sandbox = this.__sandbox
    if (!sandbox) throw Error('Sandbox was not initialized.')
    Object.entries(updates).forEach(([farId, update])=>
      sandbox.updateEl(farId, update)
    )
  }
}
