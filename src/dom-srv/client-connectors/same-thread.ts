import ClientConnectorBase from './base'
import IClientConnectorSameThread from 'types/IClientConnectorSameThread'
import IDOMConnectorSameThread from 'types/IDOMConnectorSameThread'

export default class ClientConnectorSameThread
               extends ClientConnectorBase
               implements IClientConnectorSameThread {

  protected __client?: IDOMConnectorSameThread
  protected __iAmReady: boolean = false

  constructor() {
    super()
  }

  get isReady() {
    return this.__iAmReady
  }

  set clientConn(conn: IDOMConnectorSameThread) {
    this.__client = conn
  }

  protected __specificConnector() {
    this.__iAmReady = true
    if (this.__client?.isReady) this.__weAreConnected()
    else setTimeout(()=> this.__specificConnector(), 100)
  }
}
