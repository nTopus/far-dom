import ClientConnectorBase from './client-connectors/base'
import {
  TAttrDesc,
  TTreeState,
  TTagElement,
  TConnectableTagElement,
  TConnectableNode
} from 'types/virtual-dom'
import { TUpdateDescription } from 'types/update-description'

const mkFarId = ()=> Math.random().toString(36).split('.')[1]

export default class Sandbox {

  protected __root: TConnectableTagElement
  protected __clientConnector: ClientConnectorBase
  protected __nodes: { [farId: string]: Node }

  constructor(root: TTagElement, clientConnector: ClientConnectorBase) {
    this.__root = root as TConnectableTagElement
    this.__root.farId = mkFarId()
    this.__clientConnector = clientConnector
    this.__clientConnector.connect(this)
    this.__nodes = {}
  }

  get root() {
    return this.__root
  }

  queryFullDOMState(): TTreeState {
    const elements: TTreeState = {
      [this.__root.farId]: ['BODY', '', getAttributes(this.__root)]
    }
    this.collectChildrenState(elements, this.__root)
    return elements
  }

  collectChildrenState(elements: TTreeState, parent: TConnectableTagElement) {
    Object.values(parent.childNodes as unknown as TConnectableNode[]).map(el => {
      el = el as ChildNode & { farId: string }
      if (!el.farId) el.farId = mkFarId()
      const tagEl = el as unknown as TConnectableTagElement
      const attrs = isTegElement(el) ? getAttributes(tagEl) : el.textContent
      elements[el.farId] = [ el.nodeName, parent.farId, attrs ]
      this.__nodes[el.farId] = el
      if (isTegElement(el)) this.collectChildrenState(elements, tagEl)
    })
  }

  updateEl(farId: string, update: TUpdateDescription) {
    let node = this.__nodes[farId]
    if (!node && update?.nodeName) { // It is a creation update!
      const textBuilder = update.nodeName === '#text'
                        ? 'createTextNode'
                        : update.nodeName === '#comment'
                        ? 'createComment'
                        : false
      node = this.__nodes[farId]
           = textBuilder
           ? document[textBuilder](update.text || '')
           : document.createElement(update.nodeName)
    }
    if (!node) throw Error(`What happens to farId="${farId}"?`)
    if (!update) return node.parentNode?.removeChild(node)
    if (update.parent) {
      const parent = this.__nodes[update.parent]
      if (parent) parent.appendChild(node)
      else console.error(Error(`Can't find parent "${update.parent}"!`))
    }
    if (node instanceof Element) {
      const el = node as Element
      Object.entries(update.attrs || {}).forEach(([attr, value])=> {
        if (value === null) el.removeAttribute(attr)
        else el.setAttribute(attr, value)
      })
    }
  }
}

function isTegElement(el: Node) {
  return el instanceof HTMLElement || el instanceof SVGElement
}

function getAttributes(el: Element): TAttrDesc {
  return Object.values(el.attributes)
  .reduce((attrs, att)=> ({...attrs, [att.name]: att.value}), {})
}
