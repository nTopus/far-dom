import IDOMConnectorSameThread from './IDOMConnectorSameThread'
import { TTreeState } from './virtual-dom'
import { TUpdateList } from 'types/update-description'

export default interface IClientConnectorSameThread {
  get isReady(): boolean;
  set clientConn(conn: IDOMConnectorSameThread)
  queryFullDOMState(): TTreeState
  applyUpdates(updates: TUpdateList): void
}
