import IClientConnectorSameThread from './IClientConnectorSameThread'

export default interface IDOMConnectorSameThread {
  get isReady(): boolean;
  set srvConn(conn: IClientConnectorSameThread)
}
