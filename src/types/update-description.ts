export type TAttrUpdateDesc = { [name: string]: string | null }

export type TUpdateDescription = null | {
  nodeName?: string,
  attrs?: TAttrUpdateDesc | null,
  parent?: string | null,
  text?: string | null
}

export type TUpdateList = { [farId: string]: TUpdateDescription }
