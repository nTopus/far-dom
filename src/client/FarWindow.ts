import { BasicWindowProps, FarNode, FarElement, FarAnonymousElement } from './elements/index'
import DOMConnectorBase from './dom-connectors/base'
import FarDocument from './FarDocument'
import { TTreeState, TAttrDesc } from 'types/virtual-dom'
import { TUpdateList } from 'types/update-description'

type TNonElNames = '#text' | '#comment'

const mkFarId = ()=> Math.random().toString(36).split('.')[1]

export default class FarWindow extends BasicWindowProps {

  protected __domConnector: DOMConnectorBase
  protected __doc: FarDocument
  protected __nodes: { [farId: string]: FarNode }
  protected __queuedUpdates: TUpdateList | null

  constructor(domConnector: DOMConnectorBase) {
    super()
    this.__doc = new FarDocument(this)
    this.__domConnector = domConnector
    this.__domConnector.connect(this)
    this.__nodes = {}
    this.__queuedUpdates = null
    this.pushUpdates = this.pushUpdates.bind(this)
  }

  get document() { return this.__doc }

  getFarNode(farNodeId: string) {
    return this.__nodes[farNodeId]
  }

  get farNodes() {
    return Object.values(this.__nodes)
  }

  get farElements() {
    return this.farNodes.filter(node => node instanceof FarElement) as FarElement[]
  }

  initDOMState(tree: TTreeState) {
    Object.entries(tree).forEach(([farId, elDesc])=> {
      this.createFarNode(elDesc[0], elDesc[1], farId, elDesc[2])
    })
    const event = new Event('initialized')
    this.dispatchEvent(event)
  }

  createFarNode(nodeName: string, parentFarId: string, farId: string | null, attrsOrText: TAttrDesc | string | null) {
    if (!farId) {
      // It's a local creation. Lets push to the DOM Server.
      farId = mkFarId()
      if (this.__queuedUpdates === null) this.__queuedUpdates = {}
      this.__queuedUpdates[farId] = {
        nodeName,
        parent: parentFarId,
        attrs: (typeof(attrsOrText) === 'object') ? attrsOrText : {},
        text: typeof(attrsOrText) === 'string' ? attrsOrText : null
      }
    }
    //farId = typeof(farId) === 'string' ? farId : mkFarId()
    if (nodeName[0] === '#') {
      const text = typeof(attrsOrText) === 'string' ? attrsOrText : null
      this.__nodes[farId] = new FarAnonymousElement(this, parentFarId, farId, nodeName, text)
    } else {
      let klass = (this.farTagNameMap.html as any)[nodeName] || this.HTMLUnknownElement
      this.__nodes[farId] = new klass(this, parentFarId, farId, nodeName, attrsOrText)
    }
    return this.__nodes[farId]
  }

  queueUpdateEl(farId: string, attr: string, value: string | null) {
    // TODO: update position between sibilings.
    if (this.__queuedUpdates === null) this.__queuedUpdates = {}
    if (this.__queuedUpdates[farId] === null) return
    const update = this.__queuedUpdates[farId] ??= {}
    update.attrs ??= {}
    update.attrs[attr] = value
    queueMicrotask(this.pushUpdates)
  }

  queueRemoveEl(farId: string) {
    if (this.__queuedUpdates === null) this.__queuedUpdates = {}
    this.__queuedUpdates[farId] = null
    queueMicrotask(this.pushUpdates)
  }

  pushUpdates() {
    if (!this.__queuedUpdates) return
    this.__domConnector.pushUpdates(this.__queuedUpdates)
    this.__queuedUpdates = null
  }

  removeNode(farId: string) {
    this.__removeTree(farId)
    this.queueRemoveEl(farId)
  }

  protected __removeTree(farId: string) {
    for (let id in this.__nodes)
      if (this.__nodes[id].isChildOf(farId))
        this.__removeTree(id)
    delete this.__nodes[farId]
  }
}
