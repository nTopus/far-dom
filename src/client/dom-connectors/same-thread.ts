import DOMConnectorBase from './base'
import { TTreeState } from 'types/virtual-dom'
import { TUpdateList } from 'types/update-description'
import IClientConnectorSameThread from 'types/IClientConnectorSameThread'
import IDOMConnectorSameThread from 'types/IDOMConnectorSameThread'

export default class DOMConnectorSameThread
               extends DOMConnectorBase
               implements IDOMConnectorSameThread {

  protected __srv?: IClientConnectorSameThread
  protected __iAmReady: boolean = false

  constructor() {
    super()
  }

  get isReady() {
    return this.__iAmReady
  }

  set srvConn(conn: IClientConnectorSameThread) {
    this.__srv = conn
  }

  protected __specificConnector() {
    this.__iAmReady = true
    if (this.__srv?.isReady) this.__weAreConnected()
    else setTimeout(()=> this.__specificConnector(), 100)
  }

  queryFullDOMState(): Promise<TTreeState> {
    return delay<TTreeState>(()=> {
      if (!this.__srv) throw Error('Missed to set srvConn!')
      return this.__srv.queryFullDOMState()
    })
  }

  async pushUpdates(updates: TUpdateList): Promise<void> {
    return delay(()=> {
      if (!this.__srv) throw Error('Missed to set srvConn!')
      this.__srv.applyUpdates(updates)
    })
  }
}

// Force async to better simulate the usage of all other connectors.
function delay<T>(fn: ()=> T): Promise<T> {
  return new Promise(resolve =>
    setTimeout(()=> resolve(fn()), 100)
  )
}
