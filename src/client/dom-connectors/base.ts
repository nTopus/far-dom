import FarWindow from '../FarWindow'
import { TTreeState } from 'types/virtual-dom'
import { TUpdateList } from 'types/update-description'

type NFunc = Function & { name: string }

export default class DOMConnectorBase extends EventTarget {

  protected __win?: FarWindow

  constructor() {
    super()
  }

  connect(win: FarWindow) {
    this.__win = win
    this.__specificConnector()
  }

  protected __specificConnector() {
    throw Error(`Missed "__specificConnector()" implementation in "${(this.constructor as NFunc).name}".`)
  }

  protected __weAreConnected() {
    this.queryFullDOMState()
      .then(tree => {
        if (!this.__win) throw Error('There is no Far Window. Bad initialization.')
        this.__win.initDOMState(tree)
      })
      .catch(error => {
        const event = new ErrorEvent('queryFullDOMState fail', {
          error,
          message: error.message
        })
        this.dispatchEvent(event)
      })
    const event = new Event('connected')
    this.dispatchEvent(event)
  }

  async queryFullDOMState(): Promise<TTreeState> {
    throw Error(`Missed "queryFullDOMState()" implementation in "${(this.constructor as NFunc).name}".`)
  }

  async pushUpdates(updates: TUpdateList): Promise<void> {
    throw Error(`Missed "pushUpdates()" implementation in "${(this.constructor as NFunc).name}".`)
  }
}
