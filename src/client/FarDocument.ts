import FarWindow from './FarWindow'
import FarNode from './elements/FarNode'
import FarElement from './elements/FarElement'
import { FarHTMLBodyElement } from './elements/index'
import {
  selectOne,
  selectAll,
  Adapter as IAdapter,
  Predicate as TPredicate
} from 'css-select'

export default class FarDocument {
  protected __win: FarWindow

  constructor(win: FarWindow) {
    this.__win = win
  }

  get body(): FarHTMLBodyElement {
    const body = this.__win.farElements.find(el => el instanceof FarHTMLBodyElement)
    if (!body) throw Error('No body! Did it fails on DOM Init or do you mess up?')
    return body as FarHTMLBodyElement
  }

  getElementById(id: string): FarElement | null {
    return this.__win.farElements.find(node => node.id === id) || null
  }

  querySelector(selector: string): FarElement | null {
    return selectOne<FarNode, FarElement>(selector, this.body, selectorOpts)
  }

  querySelectorAll(selector: string): FarElement[] {
    return selectAll<FarNode, FarElement>(selector, this.body, selectorOpts)
  }
}

function elMatchSelStep(el: FarElement, step: string): boolean {
  const [_, kind, name] = step.match(/^([#.]*)(.*)/) || []
  if (kind === '#') return el.id === name
  if (kind === '.') return el.classList.includes(name)
  if (kind === '') return el.tagName === name.toUpperCase()
  return false
}

const selectorOpts = {
  cacheResults: false,
  adapter: new class SelectAdapter implements IAdapter<FarNode, FarElement> {
    /**
     *  Is the node a tag?
     */
    isTag(node: FarNode): node is FarElement {
      console.log('isTag', node)
      return node instanceof FarElement
    }

    /**
     * Does at least one of passed element nodes pass the test predicate?
     */
    existsOne(test: TPredicate<FarElement>, elems: FarNode[]): boolean {
      console.error('Not Implemented')
      return false
    }

    /**
     * Get the attribute value.
     */
    getAttributeValue(elem: FarElement, name: string): string | undefined {
      console.log('getAttributeValue', elem.tagName, name)
      if (elem instanceof FarElement) return elem.getAttribute(name)
    }

    /**
     * Get the node's children < TODO: it is not "children" it is "descendants"
     */
    getChildren(node: FarNode): FarNode[] { // TODO: should be (elem: FarEl): FarEl[]
      if (!(node instanceof FarElement)) return []
      console.log('getChildren', node.nodeName, node.descendantElements)
      return node.descendantElements
    }

    /**
     * Get the name of the tag
     */
    getName(elem: FarElement): string {
      console.log('getName', (elem.tagName || '').toLowerCase())
      return (elem.tagName || '').toLowerCase()
    }

    /**
     * Get the parent of the node
     */
    getParent(node: FarElement): FarElement | null {
      console.log('getParent', node.tagName, node.parentElement)
      return node.parentElement as FarElement
    }

    /**
     * Get the siblings of the node. Note that unlike jQuery's `siblings` method,
     * this is expected to include the current node as well
     */
    getSiblings(node: FarNode): FarNode[] {
      console.error('Not Implemented')
      return []
    }

    /**
     * Get the text content of the node, and its children if it has any.
     */
    getText(node: FarNode): string {
      console.error('Not Implemented')
      return ''
    }

    /**
     * Does the element have the named attribute?
     */
    hasAttrib(elem: FarElement, name: string): boolean {
      console.error('Not Implemented')
      return false
    }

    /**
     * Takes an array of nodes, and removes any duplicates, as well as any
     * nodes whose ancestors are also in the array.
     */
    removeSubsets(nodes: FarNode[]): FarNode[] {
      console.error('Not Implemented')
      return []
    }

    /**
     * Finds all of the element nodes in the array that match the test predicate,
     * as well as any of their children that match it.
     */
    findAll(test: TPredicate<FarElement>, elems: FarNode[]): FarElement[] {
      console.log('findAll', elems.length, test.toString())
      return (elems as FarElement[]).filter(test) as FarElement[]
    }

    /**
     * Finds the first node in the array that matches the test predicate, or one
     * of its children.
     */
    findOne(test: TPredicate<FarElement>, elems: FarNode[]): FarElement | null {
      console.log('findOne', elems.length, test.toString(), (elems as FarElement[]).find(test))
      return (elems as FarElement[]).find(test) as FarElement || null
    }

    // /**
    //  * The adapter can also optionally include an equals method, if your DOM
    //  * structure needs a custom equality test to compare two objects which refer
    //  * to the same underlying node. If not provided, `css-select` will fall back to
    //  * `a === b`.
    //  */
    // equals?(a: FarNode, b: FarNode): boolean {
    //   console.error('Not Implemented')
    //   return false
    // }
    //
    // /**
    //  * Is the element in hovered state?
    //  */
    // isHovered?(elem: FarElement): boolean {
    //   console.error('Not Implemented')
    //   return false
    // }
    //
    // /**
    //  * Is the element in visited state?
    //  */
    // isVisited?(elem: FarElement): boolean {
    //   console.error('Not Implemented')
    //   return false
    // }
    //
    // /**
    //  * Is the element in active state?
    //  */
    // isActive?(elem: FarElement): boolean {
    //   console.error('Not Implemented')
    //   return false
    // }
  }
}
