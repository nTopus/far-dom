import FarElement, { TFarElementCtorArgs } from './FarElement'
export type TFarHTMLElementCtorArgs = TFarElementCtorArgs

export default class FarSVGElement extends FarElement {
  constructor(...args: TFarElementCtorArgs) {
    super(...args)
  }
}
