import FarWindow from '../FarWindow'

export default class FarNode {
  protected __farWindow: FarWindow
  protected __farParentId: string
  protected __farId: string
  protected __nodeName: string

  constructor(win: FarWindow, parentFarId: string, myFarId: string, nodeName: string) {
    this.__farWindow = win
    this.__farParentId = parentFarId
    this.__farId = myFarId
    this.__nodeName = nodeName
  }

  get parent()        { return this.__farWindow.getFarNode(this.__farParentId) }
  get parentNode()    { return this.__farWindow.getFarNode(this.__farParentId) }
  get farId()         { return this.__farId }
  get nodeName()      {
    return this.__nodeName[0] !== '#'
         ? this.__nodeName.toUpperCase()
         : this.__nodeName
  }

  get textContent(): string {
    return this.childNodes.map(node => node.textContent).join('')
  }

  get childNodes() {
    return this.__farWindow.farNodes.filter(el => el.isChildOf(this.__farId))
  }

  $descendants(filter: (node: FarNode)=> boolean = (_: FarNode)=> true): FarNode[] {
    filter // calling it only to not change the name to `_filter` and shutup linter.
           // So we can have the same signature on FarElement.
    return []
  }

  get $isStyleVisible(): boolean {
    return true
  }

  isChildOf(farId: string) {
    return farId === this.__farParentId
  }

  removeChild(child: FarNode) {
    this.__farWindow.removeNode(child.__farId)
  }

}
