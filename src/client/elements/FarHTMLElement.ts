import FarElement, { TFarElementCtorArgs } from './FarElement'
export type TFarHTMLElementCtorArgs = TFarElementCtorArgs

export default class FarHTMLElement extends FarElement {
  constructor(...args: TFarElementCtorArgs) {
    super(...args)
  }

  get $isStyleVisible(): boolean {
    const nonVisibleTags = ['STYLE', 'SCRIPT']
    return super.$isStyleVisible && !nonVisibleTags.includes(this.tagName)
  }

}
