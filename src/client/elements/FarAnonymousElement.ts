import FarWindow from '../FarWindow'
import FarNode from './FarNode'

export type TFarAnonymousElementCtorArgs = [
  win: FarWindow,
  parentFarId: string,
  myFarId: string,
  tagName: string,
  text: string | null
]

export default class FarAnonymousElement extends FarNode {
  protected __text: string

  constructor(...args: TFarAnonymousElementCtorArgs) {
    super(args[0], args[1], args[2], args[3])
    this.__text = args[4] || ''
  }

  get textContent(): string {
    return this.__text
  }

  set textContent(text: string) {
    this.__text = text
  }

}
