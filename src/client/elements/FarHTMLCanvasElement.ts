import FarHTMLElement, { TFarHTMLElementCtorArgs } from './FarHTMLElement'

export default class FarHTMLCanvasElement extends FarHTMLElement {
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}
