#!/bin/bash -e

WDIR=$(dirname "$(realpath "$0")")
ROOT="$WDIR/../../.."
DOM_D_TS="$ROOT/node_modules/typescript/lib/lib.dom.d.ts"

# TODO: Support MathML
ELEMENTS="$(
  egrep 'interface (HTML|SVG)[^ ]+Element extends (HTML|SVG)[^ ]*Element' "$DOM_D_TS" |
  sed -r 's/.*interface ((HTML|SVG)[^ ]*Element).*/\1/' |
  sort
)"

(
echo "/*
 * ATENTION!!!
 * Do not update this file by your hands. Change the 'index-updater.sh' in this
 * same dir and run 'index-updater.sh' to update this class definitions.
 * If some element class need a specific method, create (or edit) its own file.
 * When creating, you must run 'index-updater.sh' to take effect.
 */

import FarNode from './FarNode'
import FarElement from './FarElement'
import FarHTMLElement, { TFarHTMLElementCtorArgs } from './FarHTMLElement'
import FarSVGElement from './FarSVGElement'
import FarAnonymousElement from './FarAnonymousElement'
export { FarNode, FarElement, FarHTMLElement, FarSVGElement, FarAnonymousElement }"

echo "$ELEMENTS" |
while read NAME; do
  echo ''
  if test -e "$WDIR/Far$NAME.ts"; then
    echo "import Far$NAME from './Far$NAME'"
    echo "export { Far$NAME }"
  else
    echo "export class Far$NAME extends FarHTMLElement { // TODO: extend SVG!
      constructor(...args: TFarHTMLElementCtorArgs) {
        super(...args)
      }
    }" | sed 's/    //'
  fi
done

echo "
export class BasicWindowProps extends EventTarget {
  Node = FarNode
  Element = FarElement
  HTMLElement = FarHTMLElement
  SVGElement = FarSVGElement"

echo "$ELEMENTS" |
while read NAME; do
  echo "  $NAME = Far$NAME"
done

echo "  farTagNameMap = {"
echo "    html: {"
egrep '"([^"]+)":\s*HTML[^ ;]*Element;' "$DOM_D_TS" |
sed -r 's/"([^"]*)":\s*(.*);/\1 \2/' |
while read tag klass; do
  echo -n "      $tag: " | tr a-z A-Z
  echo "Far$klass,"
done
echo "    },"
echo "    svg: {"
egrep '"([^"]+)":\s*SVG[^ ;]*Element;' "$DOM_D_TS" |
sed -r 's/"([^"]*)":\s*(.*);/\1 \2/' |
while read tag klass; do
  echo -n "      $tag: " | tr a-z A-Z
  echo "Far$klass,"
done
echo "    }"
echo "  }"

echo "}"
) > "$WDIR/index.ts"
