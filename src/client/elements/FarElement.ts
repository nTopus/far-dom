import FarWindow from '../FarWindow'
import FarNode from './FarNode'
import { TAttrDesc } from 'types/virtual-dom'

export type TFarElementCtorArgs = [
  win: FarWindow,
  parentFarId: string,
  myFarId: string,
  tagName: string,
  attrs: TAttrDesc | null
]

export default class FarElement extends FarNode {
  protected __attrs: TAttrDesc

  constructor(...args: TFarElementCtorArgs) {
    super(args[0], args[1], args[2], args[3])
    this.__attrs = args[4] || {}
  }

  get tagName() { return this.__nodeName }

  get id() { return this.__attrs.id || '' }

  get parentElement() {
    return this.__farWindow.getFarNode(this.__farParentId) as FarElement
  }

  get children(): FarElement[] {
    return this.childNodes.filter((el: FarNode)=>
      el instanceof FarElement
    ) as FarElement[]
  }

  $descendants(filter: (node: FarNode)=> boolean = (_: FarNode)=> true): FarNode[] {
    return this.childNodes.filter(filter).flatMap(node => [node, ...node.$descendants(filter)])
  }

  get descendantElements(): FarElement[] {
    return this.children.flatMap(el => [el, ...el.descendantElements])
  }

  get styleObj(): { [key: string]: string } {
    const style = (this.__attrs.style || '').trim()
    if (!style) return {}
    // TODO: cache it!
    const obj: { [key: string]: string } = {}
    splitInlineStyle(style).forEach(entry => {
      const [_, key, val] = (entry.match(/([^:]+):(.*)/sm)||[]).map(s => s.trim())
      obj[snakeToCamel(`${key}`)] = normaliseStyleValue(val)
    })
    return obj
  }

  set styleObj(obj: { [key: string]: string }) {
    const style = Object.entries(obj).map(([key, value])=>
      `${camelToSnake(key)}: ${value}`
    ).join('; ')
    this.setAttribute('style', style)
  }

  get style(): { [key: string]: string } {
    return new Proxy<{ [key: string]: string }>(this as any, styleHandler as any)
  }

  get classList(): string[] {
    return []
  }

  hasAttributes() {
    return Object.keys(this.__attrs).length > 0
  }

  getAttributeNames() {
    return Object.keys(this.__attrs)
  }

  getAttribute(attr: string) {
    return this.__attrs[attr]
  }

  setAttribute(attr: string, value: string | number | boolean | null) {
    if (value === false) return this.removeAttribute(attr)
    value = `${value || ''}`
    this.__attrs[attr] = value
    this.__farWindow.queueUpdateEl(this.__farId, attr, value)
  }

  removeAttribute(attr: string) {
    this.__farWindow.queueUpdateEl(this.__farId, attr, null)
  }

  remove() {
    this.__farWindow.removeNode(this.__farId)
  }

  get textContent(): string {
    return super.textContent
  }
  set textContent(text: string) {
    this.childNodes.forEach(node => this.removeChild(node))
    this.__farWindow.createFarNode('#text', this.farId, null, text)
  }

  get innerText(): string {
    return this
    .$descendants(node => node.$isStyleVisible)
    .filter(node => node.nodeName === '#text')
    .map(node => node.textContent)
    .join('')
  }

  set innerText(text: string) {
    this.textContent = text
  }

  get innerHTML(): string {
    return this.childNodes.map(node => {
      if (node instanceof FarElement) {
        const tag = node.nodeName.toLowerCase()
        return `${mkOpenTag(node)}${node.innerHTML}</${tag}>`
      } else {
        return node.textContent
      }
    }).join('')
  }

  set innerHTML(html: string) {
    this.childNodes.forEach(node => this.removeChild(node))
    const tokens = tokenizeHTML(html)
    let curParent = this as FarElement
    tokens.forEach(token => {
      if (isTokenTag(token)) {
        if (token.tag[0] !== '/') {
          curParent = this.__farWindow.createFarNode(
            token.tag, curParent.__farId, null, token.attrs
          ) as FarElement
        } else {
          curParent = curParent.parentElement
        }
      } else {
        this.__farWindow.createFarNode('#text', curParent.__farId, null, token.text)
      }
    })
  }

  get outerHTML(): string {
    const tag = this.nodeName.toLowerCase()
    return `<${tag}>${this.innerHTML}</${tag}>`
  }

  get $isStyleVisible(): boolean {
    const style = this.style
    return style.visibility !== 'hidden' && style.display !== 'none'
  }

}

const styleHandler = {
  get(target: FarElement, prop: string) {
    return target.styleObj[prop]
  },
  set(target: FarElement, prop: string, value: string | number | null) {
    const style = target.styleObj
    if (!nullish(style[prop]) && nullish(value)) delete style[prop]
    if (!nullish(value)) style[prop] = `${value}`
    target.styleObj = style
    return value
  }
}

function splitInlineStyle(style: string): string[] {
  const chars = style.split('')
  style = ''
  let inQuote: string|null = null
  for (let i=0,c=''; c=chars[i]; i++) { // Escape (;) inside quoted strings
    if (!inQuote) { // outside quote
      if (c === '"' || c === "'") inQuote = c
    } else { // inside quote
      if (c === ';') c = '\0'
      if (c === inQuote) inQuote = null
    }
    style += c
  }
  return style.split(';').map(piece => piece.replace(/\0/g, ';'))
}

function normaliseStyleValue(val: string): string {
  const q1 = '\x00', q2 = '\x01'
  const chars = val.replace(/(?<!\\)(['"])/gsm, (s)=> s==="'" ? q1 : q2).split('')
  val = ''
  let inQuote: string|null = null
  let lastChar = ''
  for (let i=0,c=''; c=chars[i]; i++) {
    if (!inQuote) { // outside quote
      c = c.replace(/\s/sm, ' ') // normalize whitespace
      if (c === q1) { val += '"'; inQuote = q1 }
      else if (c === q2) { val += '"'; inQuote = q2 }
      else val += (lastChar === c && c === ' ') ? '' : c // Remove space repetition
      lastChar = c
    } else { // inside quote
      if ([q1, q2].includes(c)) {
        if (inQuote === q1) {
          if (c === q1) { val += '"'; inQuote = null }
          else { val += '\\"' }
        } else { //inQuote === q2
          if (c === q1) { val += "'" }
          else { val += '"'; inQuote = null }
        }
      } else if (c === '\\') {
        if (chars[i+1] === "'") c = ''
      } else {
        val += c
      }
      lastChar = ''
    }
  }
  return val
}

function nullish(obj: any) {
  return obj === null || typeof(obj) === 'undefined'
}

function camelToSnake(str: string): string {
  return str.replace(/[A-Z]/g, (chr)=> '-'+chr.toLowerCase())
}

function snakeToCamel(str: string): string {
  return str.replace(/-+(.)/g, (_, chr: string)=> chr.toUpperCase())
}

type TokenTag = { tag: string, attrs: TAttrDesc }
type TokenTxt = { text: string }
type Token = TokenTag | TokenTxt

function isTokenTag(token: Token): token is TokenTag {
  return token.hasOwnProperty('tag')
}

function tokenizeHTML(html: string): Token[] {
  const tokens: Token[] = []
  let inTag = false
  let curTag = '', curTxt = ''
  for (let chr='',i=0; chr=html[i]; i++) {
    if (inTag) {
      if (chr === '>') {
        tokens.push(tagStrToToken(curTag))
        curTag = ''
        inTag = false
      }
      else curTag += chr
    } else /* outside tag */ {
      if (chr === '<') {
        tokens.push({ text: curTxt } as TokenTxt)
        curTxt = ''
        inTag = true
      }
      else curTxt += chr
    }
  }
  // html code ended
  if (inTag) {
    // If the execution reach this point, that means it is a unclosed tag.
    // In Quirks mode we ignore that.
    tokens.push(tagStrToToken(curTag))
  } else /* outside tag */ {
    if (curTxt) tokens.push({ text: curTxt } as TokenTxt)
  }
  return tokens
}

function tagStrToToken(tagCode: string): TokenTag {
  const token = { tag: '', attrs: {} } as TokenTag
  tagCode = tagCode.trim().replace(/^<|>$/g, '')
  token.tag = tagCode.replace(/\s.*/, '')
  ;[...(
    tagCode
    .replace(/^[^\s]+\s/, '')
    .match(/([a-z0-9]+)\s*=\s*"([^"]+)"/g) || []
  )]
  .forEach(att => {
    let [_, k, v] = [...att.match(/([^=\s]+)\s*=\s*"(.*)"/) || ''] as string[]
    if (k) token.attrs[k] = v
  })
  return token
}

function mkOpenTag(el: FarElement): string {
  const tag = el.nodeName.toLowerCase()
  let attrs = ''
  el.getAttributeNames().forEach(attr => {
    attrs += ` ${attr}="${htmlEscape(el.getAttribute(attr))}"`
  })
  return `<${tag}${attrs}>`
}

function htmlEscape(value: string): string {
  return value
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&apos;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
}
