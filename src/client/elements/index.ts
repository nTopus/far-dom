/*
 * ATENTION!!!
 * Do not update this file by your hands. Change the 'index-updater.sh' in this
 * same dir and run 'index-updater.sh' to update this class definitions.
 * If some element class need a specific method, create (or edit) its own file.
 * When creating, you must run 'index-updater.sh' to take effect.
 */

import FarNode from './FarNode'
import FarElement from './FarElement'
import FarHTMLElement, { TFarHTMLElementCtorArgs } from './FarHTMLElement'
import FarSVGElement from './FarSVGElement'
import FarAnonymousElement from './FarAnonymousElement'
export { FarNode, FarElement, FarHTMLElement, FarSVGElement, FarAnonymousElement }

export class FarHTMLAnchorElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLAreaElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLAudioElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLBaseElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLBodyElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLBRElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLButtonElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

import FarHTMLCanvasElement from './FarHTMLCanvasElement'
export { FarHTMLCanvasElement }

export class FarHTMLDataElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDataListElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDetailsElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDialogElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDirectoryElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDivElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLDListElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLEmbedElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLFieldSetElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLFontElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLFormElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLFrameElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLFrameSetElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLHeadElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLHeadingElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLHRElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLHtmlElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLIFrameElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLImageElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLInputElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLLabelElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLLegendElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLLIElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLLinkElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMapElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMarqueeElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMediaElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMenuElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMetaElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLMeterElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLModElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLObjectElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLOListElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLOptGroupElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLOptionElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLOutputElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLParagraphElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLParamElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLPictureElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLPreElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLProgressElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLQuoteElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLScriptElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLSelectElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLSlotElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLSourceElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLSpanElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLStyleElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableCaptionElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableCellElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableColElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableDataCellElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableHeaderCellElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableRowElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTableSectionElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTemplateElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTextAreaElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTimeElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTitleElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLTrackElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLUListElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLUnknownElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarHTMLVideoElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGAElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGAnimateElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGAnimateMotionElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGAnimateTransformElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGAnimationElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGCircleElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGClipPathElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGComponentTransferFunctionElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGDefsElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGDescElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGEllipseElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEBlendElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEColorMatrixElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEComponentTransferElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFECompositeElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEConvolveMatrixElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEDiffuseLightingElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEDisplacementMapElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEDistantLightElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEDropShadowElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEFloodElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEFuncAElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEFuncBElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEFuncGElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEFuncRElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEGaussianBlurElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEImageElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEMergeElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEMergeNodeElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEMorphologyElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEOffsetElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFEPointLightElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFESpecularLightingElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFESpotLightElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFETileElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFETurbulenceElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGFilterElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGForeignObjectElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGGElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGGeometryElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGGradientElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGGraphicsElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGImageElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGLinearGradientElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGLineElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGMarkerElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGMaskElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGMetadataElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGMPathElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGPathElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGPatternElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGPolygonElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGPolylineElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGRadialGradientElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGRectElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGScriptElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGSetElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGStopElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGStyleElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGSVGElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGSwitchElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGSymbolElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTextContentElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTextElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTextPathElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTextPositioningElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTitleElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGTSpanElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGUseElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class FarSVGViewElement extends FarHTMLElement { // TODO: extend SVG!
  constructor(...args: TFarHTMLElementCtorArgs) {
    super(...args)
  }
}

export class BasicWindowProps extends EventTarget {
  Node = FarNode
  Element = FarElement
  HTMLElement = FarHTMLElement
  SVGElement = FarSVGElement
  HTMLAnchorElement = FarHTMLAnchorElement
  HTMLAreaElement = FarHTMLAreaElement
  HTMLAudioElement = FarHTMLAudioElement
  HTMLBaseElement = FarHTMLBaseElement
  HTMLBodyElement = FarHTMLBodyElement
  HTMLBRElement = FarHTMLBRElement
  HTMLButtonElement = FarHTMLButtonElement
  HTMLCanvasElement = FarHTMLCanvasElement
  HTMLDataElement = FarHTMLDataElement
  HTMLDataListElement = FarHTMLDataListElement
  HTMLDetailsElement = FarHTMLDetailsElement
  HTMLDialogElement = FarHTMLDialogElement
  HTMLDirectoryElement = FarHTMLDirectoryElement
  HTMLDivElement = FarHTMLDivElement
  HTMLDListElement = FarHTMLDListElement
  HTMLEmbedElement = FarHTMLEmbedElement
  HTMLFieldSetElement = FarHTMLFieldSetElement
  HTMLFontElement = FarHTMLFontElement
  HTMLFormElement = FarHTMLFormElement
  HTMLFrameElement = FarHTMLFrameElement
  HTMLFrameSetElement = FarHTMLFrameSetElement
  HTMLHeadElement = FarHTMLHeadElement
  HTMLHeadingElement = FarHTMLHeadingElement
  HTMLHRElement = FarHTMLHRElement
  HTMLHtmlElement = FarHTMLHtmlElement
  HTMLIFrameElement = FarHTMLIFrameElement
  HTMLImageElement = FarHTMLImageElement
  HTMLInputElement = FarHTMLInputElement
  HTMLLabelElement = FarHTMLLabelElement
  HTMLLegendElement = FarHTMLLegendElement
  HTMLLIElement = FarHTMLLIElement
  HTMLLinkElement = FarHTMLLinkElement
  HTMLMapElement = FarHTMLMapElement
  HTMLMarqueeElement = FarHTMLMarqueeElement
  HTMLMediaElement = FarHTMLMediaElement
  HTMLMenuElement = FarHTMLMenuElement
  HTMLMetaElement = FarHTMLMetaElement
  HTMLMeterElement = FarHTMLMeterElement
  HTMLModElement = FarHTMLModElement
  HTMLObjectElement = FarHTMLObjectElement
  HTMLOListElement = FarHTMLOListElement
  HTMLOptGroupElement = FarHTMLOptGroupElement
  HTMLOptionElement = FarHTMLOptionElement
  HTMLOutputElement = FarHTMLOutputElement
  HTMLParagraphElement = FarHTMLParagraphElement
  HTMLParamElement = FarHTMLParamElement
  HTMLPictureElement = FarHTMLPictureElement
  HTMLPreElement = FarHTMLPreElement
  HTMLProgressElement = FarHTMLProgressElement
  HTMLQuoteElement = FarHTMLQuoteElement
  HTMLScriptElement = FarHTMLScriptElement
  HTMLSelectElement = FarHTMLSelectElement
  HTMLSlotElement = FarHTMLSlotElement
  HTMLSourceElement = FarHTMLSourceElement
  HTMLSpanElement = FarHTMLSpanElement
  HTMLStyleElement = FarHTMLStyleElement
  HTMLTableCaptionElement = FarHTMLTableCaptionElement
  HTMLTableCellElement = FarHTMLTableCellElement
  HTMLTableColElement = FarHTMLTableColElement
  HTMLTableDataCellElement = FarHTMLTableDataCellElement
  HTMLTableElement = FarHTMLTableElement
  HTMLTableHeaderCellElement = FarHTMLTableHeaderCellElement
  HTMLTableRowElement = FarHTMLTableRowElement
  HTMLTableSectionElement = FarHTMLTableSectionElement
  HTMLTemplateElement = FarHTMLTemplateElement
  HTMLTextAreaElement = FarHTMLTextAreaElement
  HTMLTimeElement = FarHTMLTimeElement
  HTMLTitleElement = FarHTMLTitleElement
  HTMLTrackElement = FarHTMLTrackElement
  HTMLUListElement = FarHTMLUListElement
  HTMLUnknownElement = FarHTMLUnknownElement
  HTMLVideoElement = FarHTMLVideoElement
  SVGAElement = FarSVGAElement
  SVGAnimateElement = FarSVGAnimateElement
  SVGAnimateMotionElement = FarSVGAnimateMotionElement
  SVGAnimateTransformElement = FarSVGAnimateTransformElement
  SVGAnimationElement = FarSVGAnimationElement
  SVGCircleElement = FarSVGCircleElement
  SVGClipPathElement = FarSVGClipPathElement
  SVGComponentTransferFunctionElement = FarSVGComponentTransferFunctionElement
  SVGDefsElement = FarSVGDefsElement
  SVGDescElement = FarSVGDescElement
  SVGEllipseElement = FarSVGEllipseElement
  SVGFEBlendElement = FarSVGFEBlendElement
  SVGFEColorMatrixElement = FarSVGFEColorMatrixElement
  SVGFEComponentTransferElement = FarSVGFEComponentTransferElement
  SVGFECompositeElement = FarSVGFECompositeElement
  SVGFEConvolveMatrixElement = FarSVGFEConvolveMatrixElement
  SVGFEDiffuseLightingElement = FarSVGFEDiffuseLightingElement
  SVGFEDisplacementMapElement = FarSVGFEDisplacementMapElement
  SVGFEDistantLightElement = FarSVGFEDistantLightElement
  SVGFEDropShadowElement = FarSVGFEDropShadowElement
  SVGFEFloodElement = FarSVGFEFloodElement
  SVGFEFuncAElement = FarSVGFEFuncAElement
  SVGFEFuncBElement = FarSVGFEFuncBElement
  SVGFEFuncGElement = FarSVGFEFuncGElement
  SVGFEFuncRElement = FarSVGFEFuncRElement
  SVGFEGaussianBlurElement = FarSVGFEGaussianBlurElement
  SVGFEImageElement = FarSVGFEImageElement
  SVGFEMergeElement = FarSVGFEMergeElement
  SVGFEMergeNodeElement = FarSVGFEMergeNodeElement
  SVGFEMorphologyElement = FarSVGFEMorphologyElement
  SVGFEOffsetElement = FarSVGFEOffsetElement
  SVGFEPointLightElement = FarSVGFEPointLightElement
  SVGFESpecularLightingElement = FarSVGFESpecularLightingElement
  SVGFESpotLightElement = FarSVGFESpotLightElement
  SVGFETileElement = FarSVGFETileElement
  SVGFETurbulenceElement = FarSVGFETurbulenceElement
  SVGFilterElement = FarSVGFilterElement
  SVGForeignObjectElement = FarSVGForeignObjectElement
  SVGGElement = FarSVGGElement
  SVGGeometryElement = FarSVGGeometryElement
  SVGGradientElement = FarSVGGradientElement
  SVGGraphicsElement = FarSVGGraphicsElement
  SVGImageElement = FarSVGImageElement
  SVGLinearGradientElement = FarSVGLinearGradientElement
  SVGLineElement = FarSVGLineElement
  SVGMarkerElement = FarSVGMarkerElement
  SVGMaskElement = FarSVGMaskElement
  SVGMetadataElement = FarSVGMetadataElement
  SVGMPathElement = FarSVGMPathElement
  SVGPathElement = FarSVGPathElement
  SVGPatternElement = FarSVGPatternElement
  SVGPolygonElement = FarSVGPolygonElement
  SVGPolylineElement = FarSVGPolylineElement
  SVGRadialGradientElement = FarSVGRadialGradientElement
  SVGRectElement = FarSVGRectElement
  SVGScriptElement = FarSVGScriptElement
  SVGSetElement = FarSVGSetElement
  SVGStopElement = FarSVGStopElement
  SVGStyleElement = FarSVGStyleElement
  SVGSVGElement = FarSVGSVGElement
  SVGSwitchElement = FarSVGSwitchElement
  SVGSymbolElement = FarSVGSymbolElement
  SVGTextContentElement = FarSVGTextContentElement
  SVGTextElement = FarSVGTextElement
  SVGTextPathElement = FarSVGTextPathElement
  SVGTextPositioningElement = FarSVGTextPositioningElement
  SVGTitleElement = FarSVGTitleElement
  SVGTSpanElement = FarSVGTSpanElement
  SVGUseElement = FarSVGUseElement
  SVGViewElement = FarSVGViewElement
  farTagNameMap = {
    html: {
      A: FarHTMLAnchorElement,
      ABBR: FarHTMLElement,
      ADDRESS: FarHTMLElement,
      AREA: FarHTMLAreaElement,
      ARTICLE: FarHTMLElement,
      ASIDE: FarHTMLElement,
      AUDIO: FarHTMLAudioElement,
      B: FarHTMLElement,
      BASE: FarHTMLBaseElement,
      BDI: FarHTMLElement,
      BDO: FarHTMLElement,
      BLOCKQUOTE: FarHTMLQuoteElement,
      BODY: FarHTMLBodyElement,
      BR: FarHTMLBRElement,
      BUTTON: FarHTMLButtonElement,
      CANVAS: FarHTMLCanvasElement,
      CAPTION: FarHTMLTableCaptionElement,
      CITE: FarHTMLElement,
      CODE: FarHTMLElement,
      COL: FarHTMLTableColElement,
      COLGROUP: FarHTMLTableColElement,
      DATA: FarHTMLDataElement,
      DATALIST: FarHTMLDataListElement,
      DD: FarHTMLElement,
      DEL: FarHTMLModElement,
      DETAILS: FarHTMLDetailsElement,
      DFN: FarHTMLElement,
      DIALOG: FarHTMLDialogElement,
      DIV: FarHTMLDivElement,
      DL: FarHTMLDListElement,
      DT: FarHTMLElement,
      EM: FarHTMLElement,
      EMBED: FarHTMLEmbedElement,
      FIELDSET: FarHTMLFieldSetElement,
      FIGCAPTION: FarHTMLElement,
      FIGURE: FarHTMLElement,
      FOOTER: FarHTMLElement,
      FORM: FarHTMLFormElement,
      H1: FarHTMLHeadingElement,
      H2: FarHTMLHeadingElement,
      H3: FarHTMLHeadingElement,
      H4: FarHTMLHeadingElement,
      H5: FarHTMLHeadingElement,
      H6: FarHTMLHeadingElement,
      HEAD: FarHTMLHeadElement,
      HEADER: FarHTMLElement,
      HGROUP: FarHTMLElement,
      HR: FarHTMLHRElement,
      HTML: FarHTMLHtmlElement,
      I: FarHTMLElement,
      IFRAME: FarHTMLIFrameElement,
      IMG: FarHTMLImageElement,
      INPUT: FarHTMLInputElement,
      INS: FarHTMLModElement,
      KBD: FarHTMLElement,
      LABEL: FarHTMLLabelElement,
      LEGEND: FarHTMLLegendElement,
      LI: FarHTMLLIElement,
      LINK: FarHTMLLinkElement,
      MAIN: FarHTMLElement,
      MAP: FarHTMLMapElement,
      MARK: FarHTMLElement,
      MENU: FarHTMLMenuElement,
      META: FarHTMLMetaElement,
      METER: FarHTMLMeterElement,
      NAV: FarHTMLElement,
      NOSCRIPT: FarHTMLElement,
      OBJECT: FarHTMLObjectElement,
      OL: FarHTMLOListElement,
      OPTGROUP: FarHTMLOptGroupElement,
      OPTION: FarHTMLOptionElement,
      OUTPUT: FarHTMLOutputElement,
      P: FarHTMLParagraphElement,
      PICTURE: FarHTMLPictureElement,
      PRE: FarHTMLPreElement,
      PROGRESS: FarHTMLProgressElement,
      Q: FarHTMLQuoteElement,
      RP: FarHTMLElement,
      RT: FarHTMLElement,
      RUBY: FarHTMLElement,
      S: FarHTMLElement,
      SAMP: FarHTMLElement,
      SCRIPT: FarHTMLScriptElement,
      SEARCH: FarHTMLElement,
      SECTION: FarHTMLElement,
      SELECT: FarHTMLSelectElement,
      SLOT: FarHTMLSlotElement,
      SMALL: FarHTMLElement,
      SOURCE: FarHTMLSourceElement,
      SPAN: FarHTMLSpanElement,
      STRONG: FarHTMLElement,
      STYLE: FarHTMLStyleElement,
      SUB: FarHTMLElement,
      SUMMARY: FarHTMLElement,
      SUP: FarHTMLElement,
      TABLE: FarHTMLTableElement,
      TBODY: FarHTMLTableSectionElement,
      TD: FarHTMLTableCellElement,
      TEMPLATE: FarHTMLTemplateElement,
      TEXTAREA: FarHTMLTextAreaElement,
      TFOOT: FarHTMLTableSectionElement,
      TH: FarHTMLTableCellElement,
      THEAD: FarHTMLTableSectionElement,
      TIME: FarHTMLTimeElement,
      TITLE: FarHTMLTitleElement,
      TR: FarHTMLTableRowElement,
      TRACK: FarHTMLTrackElement,
      U: FarHTMLElement,
      UL: FarHTMLUListElement,
      VAR: FarHTMLElement,
      VIDEO: FarHTMLVideoElement,
      WBR: FarHTMLElement,
      ACRONYM: FarHTMLElement,
      APPLET: FarHTMLUnknownElement,
      BASEFONT: FarHTMLElement,
      BGSOUND: FarHTMLUnknownElement,
      BIG: FarHTMLElement,
      BLINK: FarHTMLUnknownElement,
      CENTER: FarHTMLElement,
      DIR: FarHTMLDirectoryElement,
      FONT: FarHTMLFontElement,
      FRAME: FarHTMLFrameElement,
      FRAMESET: FarHTMLFrameSetElement,
      ISINDEX: FarHTMLUnknownElement,
      KEYGEN: FarHTMLUnknownElement,
      LISTING: FarHTMLPreElement,
      MARQUEE: FarHTMLMarqueeElement,
      MENUITEM: FarHTMLElement,
      MULTICOL: FarHTMLUnknownElement,
      NEXTID: FarHTMLUnknownElement,
      NOBR: FarHTMLElement,
      NOEMBED: FarHTMLElement,
      NOFRAMES: FarHTMLElement,
      PARAM: FarHTMLParamElement,
      PLAINTEXT: FarHTMLElement,
      RB: FarHTMLElement,
      RTC: FarHTMLElement,
      SPACER: FarHTMLUnknownElement,
      STRIKE: FarHTMLElement,
      TT: FarHTMLElement,
      XMP: FarHTMLPreElement,
    },
    svg: {
      A: FarSVGAElement,
      ANIMATE: FarSVGAnimateElement,
      ANIMATEMOTION: FarSVGAnimateMotionElement,
      ANIMATETRANSFORM: FarSVGAnimateTransformElement,
      CIRCLE: FarSVGCircleElement,
      CLIPPATH: FarSVGClipPathElement,
      DEFS: FarSVGDefsElement,
      DESC: FarSVGDescElement,
      ELLIPSE: FarSVGEllipseElement,
      FEBLEND: FarSVGFEBlendElement,
      FECOLORMATRIX: FarSVGFEColorMatrixElement,
      FECOMPONENTTRANSFER: FarSVGFEComponentTransferElement,
      FECOMPOSITE: FarSVGFECompositeElement,
      FECONVOLVEMATRIX: FarSVGFEConvolveMatrixElement,
      FEDIFFUSELIGHTING: FarSVGFEDiffuseLightingElement,
      FEDISPLACEMENTMAP: FarSVGFEDisplacementMapElement,
      FEDISTANTLIGHT: FarSVGFEDistantLightElement,
      FEDROPSHADOW: FarSVGFEDropShadowElement,
      FEFLOOD: FarSVGFEFloodElement,
      FEFUNCA: FarSVGFEFuncAElement,
      FEFUNCB: FarSVGFEFuncBElement,
      FEFUNCG: FarSVGFEFuncGElement,
      FEFUNCR: FarSVGFEFuncRElement,
      FEGAUSSIANBLUR: FarSVGFEGaussianBlurElement,
      FEIMAGE: FarSVGFEImageElement,
      FEMERGE: FarSVGFEMergeElement,
      FEMERGENODE: FarSVGFEMergeNodeElement,
      FEMORPHOLOGY: FarSVGFEMorphologyElement,
      FEOFFSET: FarSVGFEOffsetElement,
      FEPOINTLIGHT: FarSVGFEPointLightElement,
      FESPECULARLIGHTING: FarSVGFESpecularLightingElement,
      FESPOTLIGHT: FarSVGFESpotLightElement,
      FETILE: FarSVGFETileElement,
      FETURBULENCE: FarSVGFETurbulenceElement,
      FILTER: FarSVGFilterElement,
      FOREIGNOBJECT: FarSVGForeignObjectElement,
      G: FarSVGGElement,
      IMAGE: FarSVGImageElement,
      LINE: FarSVGLineElement,
      LINEARGRADIENT: FarSVGLinearGradientElement,
      MARKER: FarSVGMarkerElement,
      MASK: FarSVGMaskElement,
      METADATA: FarSVGMetadataElement,
      MPATH: FarSVGMPathElement,
      PATH: FarSVGPathElement,
      PATTERN: FarSVGPatternElement,
      POLYGON: FarSVGPolygonElement,
      POLYLINE: FarSVGPolylineElement,
      RADIALGRADIENT: FarSVGRadialGradientElement,
      RECT: FarSVGRectElement,
      SCRIPT: FarSVGScriptElement,
      SET: FarSVGSetElement,
      STOP: FarSVGStopElement,
      STYLE: FarSVGStyleElement,
      SVG: FarSVGSVGElement,
      SWITCH: FarSVGSwitchElement,
      SYMBOL: FarSVGSymbolElement,
      TEXT: FarSVGTextElement,
      TEXTPATH: FarSVGTextPathElement,
      TITLE: FarSVGTitleElement,
      TSPAN: FarSVGTSpanElement,
      USE: FarSVGUseElement,
      VIEW: FarSVGViewElement,
    }
  }
}
