import defaultExport from './main'
export default defaultExport
export * from './main'

export { default as DOMConnectorBase } from './dom-connectors/base'
export { default as DOMConnectorSameThread } from './dom-connectors/same-thread'
