import { resolve } from 'path'
import { defineConfig } from 'vite'

const libPiece = process.env.PIECE
if (!libPiece) {
  console.error(`
    Missed PIECE env var. It shoud be "client" or "dom-srv"
  `)
  process.exit(1)
}

// TODO: When we have something public testable, it should export a minifyed result.
export default defineConfig({
  build: {
    emptyOutDir: false,
    lib: {
      entry: resolve(__dirname, `src/${libPiece}/full-bundle.ts`),
      formats: ['es'],
      fileName: `${libPiece}.full-bundle`
    }
  },
  esbuild: {
    minifyIdentifiers: false,
    keepNames: true
  }
})
