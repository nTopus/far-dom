import { describe, it, expect } from 'vitest'
import { setupSameThread, delay } from './common'

describe('Node::textContent', ()=> {

  it('reads a Element with multiple children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>works</strong>!</p>
    `)
    for (let root of [sandbox.root, farWin.document.body]) {
      expect(root.children[0].childNodes.length).be.equal(3)
      expect(root.children[0].textContent).be.equal('It works!')
    }
  })

  it('write a Element with children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>has children</strong>!</p>
    `)
    const farEl = farWin.document.body.children[0]
    const realEl = sandbox.root.children[0]
    expect(farEl.childNodes.length).be.equal(3)
    farEl.textContent = 'It also works!'
    await delay(.2) // Give some time to send the update to realEl.
    expect(farEl.childNodes.length).be.equal(1)
    expect(realEl.childNodes.length).be.equal(1)
    expect(farEl.textContent).be.equal('It also works!')
    expect(realEl.textContent).be.equal('It also works!')
  })

})

describe('Node::innerText', ()=> {

  it('reads a Element with multiple children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>works</strong>!</p>
    `)
    const root = farWin.document.body
    expect(root.children[0].childNodes.length).be.equal(3)
    console.log(root.outerHTML, root.children[0].tagName, root.children[0].innerText)
    expect(root.children[0].innerText).be.equal('It works!')
  })

  /*
   * https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent#differences_from_innertext
   */
  describe('cant read nonvisible elements', ()=> {
    it.each(['script', 'style'])
    ('Cant read from %s tag', async (tag)=> {
      const { farWin } = await setupSameThread().initSandbox(`
        <p>It <${tag}>must</${tag}> <strong>works</strong>!</p>
      `)
      const el = farWin.document.body.children[0]
      expect(el.childNodes.length).be.equal(5)
      expect(el.innerText).be.equal('It  works!')
    })

    it.each(['display:none', 'visibility:hidden'])
    ('Cant read from %s styled element', async (style)=> {
      const { farWin } = await setupSameThread().initSandbox(`
        <p>It <span style="${style}">must</span> <strong>works</strong>!</p>
      `)
      const el = farWin.document.body.children[0]
      expect(el.childNodes.length).be.equal(5)
      expect(el.innerText).be.equal('It  works!')
    })
  })

  it('write an Element with children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>has children</strong>!</p>
    `)
    const farEl = farWin.document.body.children[0]
    const realEl = sandbox.root.children[0]
    expect(farEl.childNodes.length).be.equal(3)
    farEl.innerText = 'It also works!'
    expect(farEl.childNodes.length).be.equal(1)
    expect(farEl.innerText).be.equal('It also works!')
    await delay(.2) // Give some time to send the update to realEl.
    expect(realEl.childNodes.length).be.equal(1)
    // JSDOM don't have innerText https://github.com/jsdom/jsdom/issues/1245
    // So, in this case, as it should be the same, we can use textContent.
    expect(realEl.textContent).be.equal('It also works!')
  })

})

describe('Node::innerHTML', ()=> {

  it('reads a Element with multiple children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>works</strong>!</p>
    `)
    for (let root of [sandbox.root, farWin.document.body]) {
      expect(root.children[0].childNodes.length).be.equal(3)
      expect(root.children[0].innerHTML).be.equal('It <strong>works</strong>!')
    }
  })

  it('reads a Element with style, data attribute and unknown attribute', async ()=> {
    const html = `<p style="color:red" data-foo="bar" abc="def">Test</p>`
    const { farWin, sandbox } = await setupSameThread().initSandbox(html)
    for (let root of [sandbox.root, farWin.document.body]) {
      expect(root.innerHTML).be.equal(html)
    }
  })

  it('write a Element with children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>has children</strong>!</p>
    `)
    const farEl = farWin.document.body.children[0]
    const realEl = sandbox.root.children[0]
    expect(farEl.childNodes.length).be.equal(3)
    farEl.innerHTML = 'It <i>also</i> <strong>works</strong>!'
    expect(farEl.childNodes.length).be.equal(5)
    expect(farEl.innerHTML).be.equal('It <i>also</i> <strong>works</strong>!')
    await delay(.2) // Give some time to send the update to realEl.
    expect(realEl.childNodes.length).be.equal(5)
    expect(realEl.innerHTML).be.equal('It <i>also</i> <strong>works</strong>!')
  })

})

describe('Node::outerHTML', ()=> {

  it('reads a Element with multiple children', async ()=> {
    const { farWin, sandbox } = await setupSameThread().initSandbox(`
      <p>It <strong>works</strong>!</p>
    `)
    for (let root of [sandbox.root, farWin.document.body]) {
      expect(root.children[0].childNodes.length).be.equal(3)
      expect(root.children[0].outerHTML).be.equal('<p>It <strong>works</strong>!</p>')
    }
  })

})
