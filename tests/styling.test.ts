import { describe, it, expect } from 'vitest'
import { setupSameThread } from './common'

async function createEl(style: string='') {
  const { farWin } = await setupSameThread().initSandbox(
    style ? `<p style="${style}">Ok</p>` : `<p></p>`
  )
  const el = farWin.document.querySelector('p')
  if (!el) throw Error('<p> was not found.')
  return el
}

describe('read inline element styles', ()=> {
  it('read multi-word attribute name', async ()=> {
    const el = await createEl(`border-left-color: red`)
    expect(el.style.borderLeftColor).be.equal('red')
  })

  it('translate string protected value from single quote to double quotes', async ()=> {
    const el = await createEl(`font: 'My Font', Arial, 'Other font'`)
    expect(el.style.font).be.equal('"My Font", Arial, "Other font"')
  })

  it('translate string protected value without mess single quotes inner double quotes', async ()=> {
    const el = await createEl(`font: &quot;Ada's Font&quot;, 'Bob\\'s Font', &quot;'Other' font&quot;`)
    expect(el.style.font).be.equal('"Ada\'s Font", "Bob\'s Font", "\'Other\' font"')
  })

  it('translate string protected value escaping double quotes inside single quotes', async ()=> {
    const el = await createEl(`content: 'This is a &quot;test&quot;'`)
    expect(el.style.content).be.equal('"This is a \\"test\\""')
  })

  it('translate string protected value &quot;', async ()=> {
    const el = await createEl(`font: &quot;Times New Roman&quot;`)
    expect(el.style.font).be.equal('"Times New Roman"')
  })

  it('Read multiple values', async ()=> {
    const el = await createEl(`font: 22px 'Times New Roman'`)
    expect(el.style.font).be.equal('22px "Times New Roman"')
  })

  it('Adjust value spacing', async ()=> {
    const el = await createEl(`font: bold\n22px   'My  Font'`)
    expect(el.style.font).be.equal('bold 22px "My  Font"')
  })

  it('read multiple args', async ()=> {
    const el = await createEl(`content: 'Hi!'; color: red; font: Arial`)
    expect(el.style.content).be.equal('"Hi!"')
    expect(el.style.color).be.equal('red')
    expect(el.style.font).be.equal('Arial')
  })

  it('read multiple args, with a (;) in a quoted string', async ()=> {
    const el = await createEl(`content: 'Hi; Hello'; color: red`)
    expect(el.style.content).be.equal('"Hi; Hello"')
    expect(el.style.color).be.equal('red')
  })
})

describe('write inline element styles', ()=> {
  it('write one simple attribute', async ()=> {
    const el = await createEl()
    el.style.color = 'red'
    expect(el.getAttribute('style')).be.equal('color: red')
  })

  it('write one multi-word attribute', async ()=> {
    const el = await createEl()
    el.style.borderLeftColor = 'red'
    expect(el.getAttribute('style')).be.equal('border-left-color: red')
  })
})
