import { it, expect } from 'vitest'
import { setupSameThread } from './common'

it('initializes with empty sandbox', async ()=> {
  const { farWin, sandbox } = await setupSameThread().initSandbox('')
  expect(sandbox.root.farId).to.match(/^[0-9a-z]+$/)
  expect(sandbox.root.childNodes.length).be.equal(0)
  expect(farWin.document.body.innerHTML).be.equal('')
})

it('initializes sandbox with some html', async ()=> {
  const { farWin, sandbox } = await setupSameThread().initSandbox(`
    <h1>This is a Test</h1>
    <p>And it <strong>works</strong>!</p>
  `)
  expect(farWin.document.body.innerHTML).be.equal(`
    <h1>This is a Test</h1>
    <p>And it <strong>works</strong>!</p>
  `)
  for (let root of [sandbox.root, farWin.document.body]) {
    expect(root.childNodes.length).be.equal(5)
    expect(root.children.length).be.equal(2)
    expect(root.children[1].childNodes.length).be.equal(3)
  }
})

it('have different element constructors', async ()=> {
  const { farWin, sandbox } = await setupSameThread().initSandbox(`
    <h1>This is a Test</h1>
    <p>And it <strong>works</strong>!</p>
  `)
  const sbRoot = sandbox.root
  const farBody = farWin.document.body
  expect(sbRoot.children[0].constructor.name).be.equal('HTMLHeadingElement')
  expect(farBody.children[0].constructor.name).be.equal('FarHTMLHeadingElement')
  expect(sbRoot.children[1].constructor).be.equal(HTMLParagraphElement)
  expect(farBody.children[1].constructor).be.equal(farWin.HTMLParagraphElement)
})

it('gets element by id', async ()=> {
  const { farWin } = await setupSameThread().initSandbox(`
    <h1 id="test1">This is a Test</h1>
    <p>And it <strong id="test2">works</strong>!</p>
  `)
  expect(farWin.document.getElementById('test1')?.tagName).be.equal('H1')
  expect(farWin.document.getElementById('test2')?.tagName).be.equal('STRONG')
})
