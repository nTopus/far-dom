import FarWindow from '../src/client/main'
import ClientToSrvConn from '../src/client/dom-connectors/same-thread'
import Sandbox from '../src/dom-srv/main'
import SrvToClientConn from '../src/dom-srv/client-connectors/same-thread'

type TContextExtensionLvl1 = {
  farWin: FarWindow,
  initSandbox(innerHTML: string): Promise<TContextExtensionLvl2>
}

type TContextExtensionLvl2 = TContextExtensionLvl1 & {
  sandbox: Sandbox
}

export function setupSameThread(origCtx: Record<string, any> = {}) {
  const ctx1 = origCtx as TContextExtensionLvl1
  const clientToSrv = new ClientToSrvConn()
  const srvToClient = new SrvToClientConn()

  clientToSrv.srvConn = srvToClient
  srvToClient.clientConn = clientToSrv

  ctx1.farWin = new FarWindow(clientToSrv)

  ctx1.initSandbox = function initSandbox(innerHTML: string): Promise<TContextExtensionLvl2> {
    const ctx2 = ctx1 as TContextExtensionLvl2
    const root = document.createElement('div')
    root.innerHTML = innerHTML
    ctx2.sandbox = new Sandbox(root, srvToClient)
    return new Promise(resolve =>
      ctx2.farWin.addEventListener('initialized', ()=> resolve(ctx2))
    )
  }

  return ctx1
}

export function delay(secs: number) {
  return new Promise(resolve => setTimeout(resolve, secs*1000))
}
